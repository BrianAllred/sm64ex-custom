# sm64ex-custom

This is a custom PKGBUILD for building sm64ex on Arch-based Linux distributions.

It's not in the AUR because this PKGBUILD aims to be as customizable as possible. Check out Hekuran's [sm64pc-git](https://aur.archlinux.org/packages/sm64pc-git/) package.

Big thanks to Hekuran for the PKGBUILD base, [sm64pcBuilder2](https://sm64pc.info/sm64pcbuilder2/) (gunvalk & Filipianosol) for interface inspiration, and [TK-Glitch](https://github.com/Tk-Glitch/PKGBUILDS) for the customization inspiration!

## Usage

Simply run `git clone https://gitlab.com/BrianAllred/sm64ex-custom.git` and then `makepkg` in the cloned directory.

Building in a clean chroot is possible, even when building for Windows. However, for Windows builds, you'll have to do one of the following:

1. Manually specify the required MinGW packages on the command line (and have the packaged MinGW tar files handy)
2. Have the packages hosted in a repo, use a custom pacman.conf inside the chroot, and move the packages from `optdepends` to `makedepends`

## Customization

For quicker builds, specify options beforehand with the customization.cfg in the build directory or the config file in your config directory (default at `~/.config/sm64ex-custom/config`). If the config file doesn't exist yet, copy the sample customization.cfg to that destination (or another desired destination) and modify as desired.

The customization.cfg is pretty straightforward. Note that if your config file lives in a non-default location, make sure to specify that location in the customization.cfg in the build directory.

See the [sm64ex wiki](https://github.com/sm64pc/sm64ex/wiki/Build-options) for more info on the build options themselves.

## Cross-compilation

Compiling for Windows/Wine **is** supported. Compiling with D3D **is** supported. Make sure the proper optdepends are installed beforehand. Easiest thing to do is simply run `makepkg -s`, specify your build options, then extract the contents of the tar to the desired Windows machine.

Note that if your Windows build targets the OpenGL rendering API, the appropriate glew32.dll will be copied to the output directory during packaging.

## Credits

Special thanks to the content creators of the various mods, models, and texture packs included in this project. If you're missing, please raise an issue or pull request!

### Mods

#### 60 FPS: sm64ex/sm64pc teams

Separates the graphics rendering from the game timer in order to render at 60 FPS and keep the game logic at 30.

#### Stay in level after star (non-stop mode): GateGuy

Stay in the level after obtaining a star. May cause issues when catching Mips.

#### Captain Toad: Keanine

Turn the star-giving Toads in the castle into Captain Toads.

#### Return to title from ending: GateGuy

Return to title screen / main menu after ending (instead of starting back in front of the castle).

#### 3D Coin Patch: TzKet-Death & grego2d

Turn coins into 3D models instead of sprites. Pretty, but doesn't match some texture packs.

#### 50 coin 1-UPs: Keanine

Traditionally, the game only awarded 1-UPs at 50, 100, and 150 coin intervals. This patch fixes it so they are awarded on any interval divisible by 50.

#### Exit to main menu from options: Adya

Add an "Exit to Main Menu" option in the options menu (Pause -> R (or R_Shift)).

#### Increase delay on star select: GateGuy

Increases the time between select a course star and starting the course, allowing the player to hear Mario say the full line "Let's-a Go!".

### Models

[HD Mario](http://sm64pc.info/forum/viewtopic.php?f=2&t=3)

[Hat Kid](https://sm64pc.info/forum/viewtopic.php?f=2&t=20)

[Mawio](https://sm64pc.info/forum/viewtopic.php?f=9&t=22)

[HD Bowser](http://sm64pc.info/forum/viewtopic.php?f=2&t=30)

Old School HD Mario: Xinus22, ported by DorfDork

[SGI Project](https://discord.gg/J79VRGU)

### Textures

[MollyMutt's Texture Pack](http://www.emutalk.net/threads/43773-mollymutt-s-100-finished-super-mario-64-pack)

[Hypatia's Mario Craft 64](http://sm64pc.info/forum/viewtopic.php?f=3&t=29)

[SM64 Redrawn](https://github.com/TechieAndroid/sm64redrawn)

[OwOify Project](https://sm64pc.info/forum/viewtopic.php?f=9&t=22)

[p3st Texture Pack](https://github.com/p3st-textures/p3st-Texture_pack)

[RESRGAN-16xre-upscale Texture Pack](https://github.com/pokeheadroom/RESRGAN-16xre-upscale-HD-texture-pack)

[Cleaner Aesthetics](https://github.com/CrashCrod/Cleaner-Aesthetics)
